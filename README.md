# ![linux](linux.png) **Les Commandes linux**

la commande ultime : ``man``

# **Naviguer dans les répertoires**

+ ``pwd``         affiche le répertoire courant
+ ``cd rep``      se place dans le répertoire rep
+ ``cd``          se place dans le répertoire de l'utilisateur ~/
+ ``cd ..``       se place dans le répertoire parent
+ ``ls rep``      liste les fichiers du répertoire rep
+ ``ls -a``       ls avec les fichiers cachés
+ ``ls -l``       ls avec les droits d'accès et la taille

# **Actions sur les fichiers/dossiers**

+ ``mv source cible``       deplace le fichier source vers cible
+ ``cp source cible``       copie le fichier source vers cible
+ ``cp -R source cible``    copie le répertoire source vers cible
+ ``ln source lien``        créer un lien fort de source vers lien
+ ``ln -s source lien``     créer un lien symbolique de source vers lien
+ ``touch file ``           créer le fichier file ou met à jour sa date de modification
+ `` mkdir rep``            créer un repertoire rep
+ ``mkdir -p rep/rep2``     mkdir avec création du rep parent si nécessaire
+ ``rm file``              supprime le fichier file
+ ``rm -f file``            supprime le fichier file protégé en écriture
+ ``rmdir rep``             supprimer un répertoire vide
+ ``rm -R rep``            supprime un répertoire
+ ``du -h file ou rep``     affiche la taille de file ou du répertoire rep

# **Afficher/Comparer les fichiers**

+ ``wc fichier``            compte le nombre de lignes, de mots, d'octets de fichier
+ ``cat fichiers``          concatène les fichiers
+ ``more fichier``          affiche fichier page après page 'Espace'=page suivante, 'Entrée'=ligne suivante, 'u'=remonter
+ ``less fichier``          affiche *fichier* avec une navigation au clavier
+ ``head -n x fichier``     affiche les x premières lignes de fichier
+ ``tail -n x fichier``     affiche les x dernières lignes de fichier
+ ``tail -f fichier``       affiche la dernière ligne de fichier en temps réel
+ ``diff file1 file2 ``     affiche les différences entre deux fichiers texte
+ ``diff -u file1 file2``   affiche les différences au format patch
+ ``comp file1 file2 ``     compare deux fichiers binaires
+ ``comp file1 file2 n N `` compare deux fichiers, file1 à partir du nième octet, et *file2* à partir du **N**ième
                      
# **Utilisateurs**

+ ``whoami``      affiche le login de l'utilisateur
+ ``who``          affiche les utilisateurs connectés
+ ``id``           afficher les uid, gid et groupes de l'utilisateur
+ ``id user``      afficher les uid, gid et groupes de user (root only)
+ ``finger user``  affiche les informations de user
+ ``write user``   afficher un message sur le terminal de user
+ ``tty``          afficher le nom de son terminal
+ ``su - sudo``   passer en mode administrateur, super-utilisateur
+ ``passwd``      changer le mot de passe de l'utilisateur courant
+ ``adduser``      ajouter un utilisateur
+ ``deluser``      supprime un utilisateur
+ ``addgroup``     ajoute un groupe
+ ``delgroup``     supprime un groupe

# **Processus**

+ ``ps``                  afficher les processus de l'utilisateur
+ ``ps ax``               afficher tous les processus
+ ``ps aux``              afficher tous les processus et leur utilisateur
+ ``pstree``              afficher les processus dans une arborescence
+ ``top``                 afficher un tableau des processus gourmands
+ ``kill signal pid``     tuer un processus en utilisant son pid
+ ``pkill signal nom``    tuer un processus en utilisant le nom du programme

# **signaux utilisés par kill/pkill**

+ ``-1``       (HUP)    recharger le fichier de configuration du processus
+ ``-2``       (INT)    interrompre le processus
+ ``-3``       (QUIT)   quitter le processus
+ ``-9``       (KILL)   tuer le processus (à eviter, tenter -15 avant)
+ ``-15``      (TERM)   terminer le processus proprement
+ ``-18``      (STOP)   geler le processus
+ ``-20``      (CONT)   reprendre l'exécution d'un processus gelé

# **Matériel**

+ ``lsusb``                 liste les périphériques de type USB connectés
+ ``lspci``                  liste les périphériques de type PCI connectés
+ ``cat /proc/cpuinfo``      affiche les informations processeur
+ ``cat /proc/partitions``   affiche les partitions montées


**afficher le modèle de sa carte graphique :**

+ lspci | egrep "3D|Display|VGA"

**afficher le modèle de sa carte Wi-fi :**

+ lspci | grep -i "net" | cut -d: -f3

**afficher le modèle de sa carte son :**

+ lspci | grep -i audio | cut -d: -f3


# **Réseau**

+ ``hostname``                      affiche le nom d'hôte de la machine
+ ``ping 'machine'``                envoie un ping à une 'machine'
+ ``traceroute 'machine'``          fait un traceroute vers 'machine'
+ ``netstat``                       liste les processus utilisant le réseau
+ ``netstat -a``                    netstat + affichage des processus serveurs
+ ``lsof``                          liste détaillée de l'usage des fichiers et du réseau
+ ``ifconfig``                      affiche la config des interfaces réseaux
+ ``ifconfig interface IP masque``  configure une interface réseau
+ ``route``                         affiche la table de routage
+ ``curl ifconfig.me``              IP publique


+ ip address show eth0 | grep "inet " | tr -s " " ":" | cut -d: -f3
+ /sbin/ifconfig eth0 | grep "inet " | tr -s " " ":" | cut -d: -f4
+ ip address show eth0 | grep "inet " | tr -s " " ":" | tr -s "/" ":" | cut -d: -f3

# **Recherche**

+ ``locate motif``         recherche sur un nom correspond au motif
+`` updatedb``             mettre à jour la base de données de locale
+ ``find chemin options``  recherche les fichiers dans chemin avec option
+ ``find -name motif``     recherche sur le nom du fichier
+ ``find -type f/d/l``     recherche par type où f=fichier,d=répertoire,l=lien
+ ``find -exec cmd``       exécute la commande cmd à tous les fichiers trouvés

+ find $HOME/Images -name "*.png" -exec cp {} $HOME/tmp/ \;

# **Archives**

+ ``.tar.bz2, .tbz2``  tar -cvjf archive.tar.bz2 repertoire    tar xvjf
+ ``.tar.gz, .tgz``    tar -cvzf archive.tar.gz repertoire     tar xvzf
+ ``.bz2``            bzip2 fichiers                          bunzip2
+ ``.rar``             -                                       unrar x
+ ``.gz``              gzip fichiers                           gunzip
+ ``.tar``             tar -cvf archive.tar fichiers           tar xvf
+ ``.zip``             zip -r archive.zip fichiers             unzip
+ ``.Z``               compress fichiers                       uncompress
+ ``.7z``              7z a fichiers                           7z x
+ ``.xz``              xz -z repertoire                        unxz

